# rebootinfo

Sends an email before shutdown and after reboot to root.

The package is available for `debian` and `ubuntu` on
[https://repo.data.kit.edu](https://repo.data.kit.edu)
