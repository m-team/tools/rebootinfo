VERSION := $(shell head debian/changelog  -n 1 | cut -d \( -f 2 | cut -d \) -f 1 | cut -d \- -f 1)
RELEASE := $(shell head debian/changelog  -n 1 | cut -d \( -f 2 | cut -d \) -f 1 | cut -d \- -f 2)
PACKAGE := $(shell head debian/changelog  -n 1 | cut -d \  -f 1 )
ARCH := "all"

all: install

.PHONY: info
info:
	@echo "${PACKAGE} - ${VERSION} - ${RELEASE}"
	@echo "DESTDIR: ${DESTDIR}"

.PHONY: debsource
debsource: distclean
	@quilt pop -a || true
	@debian/rules clean
	( cd ..; tar czf ${PACKAGE}_${VERSION}.orig.tar.gz --exclude-vcs --exclude=debian --exclude=.pc ${PACKAGE})

.PHONY: install
install:
	@install -D -d -m 755 $(DESTDIR)/usr/lib/rebootinfo
	@install -D -m 644 rebootinfo-start.mail $(DESTDIR)/usr/lib/rebootinfo/rebootinfo-start.mail
	@install -D -m 644 rebootinfo-stop.mail $(DESTDIR)/usr/lib/rebootinfo/rebootinfo-stop.mail
	@install -D -m 755 rebootinfo $(DESTDIR)/usr/bin/rebootinfo
	@install -D -m 644 rebootinfo.service $(DESTDIR)/lib/systemd/system/rebootinfo.service

.PHONY: clean
clean:
	@echo "CLEAN: rm -rf $(DESTDIR)"

.PHONY: distclean
distclean: clean

.PHONY: deb
deb: debsource
	dpkg-buildpackage -uc -us

.PHONY: upload
upload: deb
	scp ../$(PACKAGE)_$(VERSION)-$(RELEASE)_$(ARCH).deb root@repo.data.kit.edu:/var/www/debian/buster
	scp ../$(PACKAGE)_$(VERSION)-$(RELEASE)_$(ARCH).deb root@repo.data.kit.edu:/var/www/debian/bullseye
	scp ../$(PACKAGE)_$(VERSION)-$(RELEASE)_$(ARCH).deb root@repo.data.kit.edu:/var/www/ubuntu/bionic
	scp ../$(PACKAGE)_$(VERSION)-$(RELEASE)_$(ARCH).deb root@repo.data.kit.edu:/var/www/ubuntu/focal
